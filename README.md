# SHU Teaching Git Example

## Git clone right from Python

### Install gitpython first from CLI or Terminal
`pip install gitpython`

### Go to Jupyter Notebook 
```
# Import the necessary module
import git

# Define the repository URL
repo_url = 'https://gitlab.com/lucibelloj/shu_teaching.git'

# Define the directory where the repo will be cloned
clone_dir = '/path/to/clone/directory'  # Update this to your desired path
example = 'clone_dir = '/Users/josephlucibello/Desktop/shu_teaching_clone'

# Define the directory where the repo will be cloned
clone_dir = '/path/to/clone/directory'  # Update this to your desired path

# Clone the repository
git.Repo.clone_from(repo_url, clone_dir)

print(f'Repository cloned to {clone_dir}')

```